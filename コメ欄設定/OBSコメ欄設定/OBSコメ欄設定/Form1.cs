﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OBSコメ欄設定
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }

    class test
    {
        private void csv_output_timer_Tick(object sender, EventArgs e)
        {
            if (outputFolder.Text != "")
            {
                string textfile = outputFolder.Text;
                string write_data;

                if (!System.IO.File.Exists(textfile))
                {
                    Directory.CreateDirectory(textfile);
                    using (FileStream fs = File.Create(textfile + "\\data.csv")) ;
                }
                textfile += "\\data.csv";
                AddFileSecurity(textfile, account.Text, FileSystemRights.FullControl, AccessControlType.Allow);


                ClearTextFile(textfile);

                // 文字コードを指定
                Encoding enc = Encoding.GetEncoding("UTF-8");

                // ファイルを開く
                StreamWriter writer = new StreamWriter(@textfile, true, enc);

                // テキストを書き込む

                write_data = Csv_Maker();

                //改行なし
                writer.Write(write_data);

                // ファイルを閉じる
                writer.Close();
            }
        }

        public static void AddFileSecurity(string fileName, string account, FileSystemRights rights, AccessControlType controlType)
        {
            // Get a FileSecurity object that represents the
            // current security settings.
            FileSecurity fSecurity = File.GetAccessControl(fileName);

            // Add the FileSystemAccessRule to the security settings.
            fSecurity.AddAccessRule(new FileSystemAccessRule(account, rights, controlType));

            // Set the new access settings.
            File.SetAccessControl(fileName, fSecurity);
        }

        public void ClearTextFile(string filePathName)
        {
            using (var fileStream = new FileStream(filePathName, FileMode.Open))
            {
                // ストリームの長さを0に設定します。
                // 結果としてファイルのサイズが0になります。
                fileStream.SetLength(0);
            }
        }
    }
}
